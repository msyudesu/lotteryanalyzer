import sys
import os
import pandas as pd

class LotteryAnalyzer():
    

    def generate_dataframes(self, lottery_numbers_excel):
        self.lottery_df = pd.read_excel(lottery_numbers_excel, sheet_name="White Numbers")
        self.powerball_df = pd.read_excel(lottery_numbers_excel, sheet_name="Powerball Numbers")

    def top_ten_numbers(self):
        temp_df = self.lottery_df.sort_values('Counter', ascending=False)
        return list(temp_df['Number'][0:10])

    def top_five_powerball_numbers(self):
        temp_df = self.powerball_df.sort_values('Counter', ascending=False)
        return list(temp_df['Number'][0:5])
    
    def output_top_results(self):
        top_powerball_numbers = (self.powerball_df.sort_values('Counter', ascending=False)).head(5)
        top_regular_numbers = (self.lottery_df.sort_values('Counter', ascending=False)).head(10)

        top_regular_numbers.to_excel("reg_esults.xlsx", sheet_name="Regular Numbers", index=False)
        top_powerball_numbers.to_excel("pb_results.xlsx", sheet_name="Powerball Numbers", index=False)



def main(filepath):
    if os.path.exists(filepath):
        lottery_numbers_excel = filepath
        la = LotteryAnalyzer()
        la.generate_dataframes(lottery_numbers_excel)
        print(f"Top 10 Regular Numbers: {la.top_ten_numbers()}")
        print(f"Top 5 Powerball Numbers: {la.top_five_powerball_numbers()}")
        la.output_top_results()
    else:
        print("File does not exist.")

if __name__ == "__main__":
    try:
        main(sys.argv[1])
    except IndexError:
        print("No lottery results file specified.")